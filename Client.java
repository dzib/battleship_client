import java.io.*;
import java.net.*;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("localhost", 8000);
        InputStream input = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(input));
        OutputStream output = socket.getOutputStream();
        PrintWriter writer = new PrintWriter(output, true);

        Scanner scanner = new Scanner(System.in);
        String text;
        do {
            System.out.print("Client: ");
            text = scanner.nextLine();
            writer.println(text);
            System.out.println("Server: " + reader.readLine());
        } while (!text.equals("bye"));

        socket.close();
    }
}